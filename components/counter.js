import { connect } from 'react-redux'
import makeStore from '../store';

const Counter = (props) => (
  <div id="counter">
    <h1>{props.getState().counter}</h1>
    <div className="action-btns">
      <button
      onClick={() => props.dispatch({type:'INC'})}
      >+</button>
      <button
      onClick={() => props.dispatch({type:'DEC'})}
      >
        -
      </button>
    </div>
  </div>
)

export default connect(makeStore)(Counter)