import { createStore, combineReducers } from 'redux'

import counterReducer from './counter/reducer'

const rootReducer = combineReducers({
  counter: counterReducer
})

const makeStore = (initialState, opt) => {
  return createStore(rootReducer, initialState)
}

export default makeStore